# Changelog

## [v2.1.0]

- Feature #27999 [StorageHub] downstream components to upgrade in order to work with storagehub 1.5.0
- moved to gcube-smartgears-bom 2.5.1


## [v2.0.1] - 2024-04-16

- fixed dependency versions


## [v2.0.0] - 2023-12-04

- Integrated corresponding API calls for all the public functions in Social Networking Library

## [v1.2.0] - 2022-10-20

 - Minor fix on a method name
 - Feature #23887 possibility to get-usernames-by-role in UserClient
 - Feature #23995 Social Service add support for set Message read / unread

## [v1.1.0] - 2022-04-06

 - First release
 - Feature #23186 - Full notifications support for social service


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).