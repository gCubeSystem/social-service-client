package org.gcube.social_networking.social_networking_client_library;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.MessageInbox;
import org.gcube.social_networking.socialnetworking.model.beans.MessageInputBean;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Messages client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class MessageClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/messages/";
	private static Logger logger = LoggerFactory.getLogger(MessageClient.class);


	public MessageClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}

	/**
	 * Get sent messages
	 * @return
	 */
	public List<MessageInbox> getSentMessages(){

		logger.debug("Request for sent messages");
		String thisMethodSignature = "get-sent-messages";
		String request =  getServiceEndpoint()  + thisMethodSignature + "?";
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<MessageInbox>>>(){}, request);

	}

	/**
	 * Get received messages
	 * @return
	 */
	public List<MessageInbox> getReceivedMessages(){

		logger.debug("Request for received messages");
		String thisMethodSignature = "get-received-messages";
		String request =  getServiceEndpoint()  + thisMethodSignature + "?";
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<MessageInbox>>>(){}, request);

	}

	/**
	 * Write message
	 * @return message id
	 */
	public String writeMessage(MessageInputBean m){

		Validate.isTrue(m != null, "Message cannot be null");

		logger.debug("Request for writing new message");
		String thisMethodSignature = "write-message";
		String request =  getServiceEndpoint() + thisMethodSignature + "?";
		return HttpClient.post(new GenericType<ResponseBean<String>>(){}, request, m);

	}

	/**
	 * mark the message read or not
	 * @param messageId the message identifier
	 * @param read true to set read, false to set unread
	 * @return the result of the operation
	 */
	public String markMessageRead(String messageId, boolean read) {

		Validate.isTrue(messageId != null, "Message id cannot be null");

		logger.debug("set Message id:" + messageId + " read?" + read);	
		String thisMethodSignature = "set-message-read";
		String request =  getServiceEndpoint() + thisMethodSignature;
		
		Form form = new Form();
		form.param("messageId", messageId);
		form.param("read", read+"");
		
		return HttpClient.postFormData(new GenericType<ResponseBean<String>>(){}, request, form);

	}
}
