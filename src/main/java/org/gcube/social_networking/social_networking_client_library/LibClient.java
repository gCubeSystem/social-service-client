package org.gcube.social_networking.social_networking_client_library;

import org.apache.commons.lang.Validate;
import org.gcube.portal.databook.shared.*;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.GenericType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibClient extends BaseClient{

    private static final String SUB_SERVICE_PATH = "2/lib/";
    private static Logger logger = LoggerFactory.getLogger(LibClient.class);


    public LibClient() throws Exception {
        super(SUB_SERVICE_PATH);
    }

    //Comments
    public Boolean addCommentLib(Comment comment){
        Validate.isTrue(comment != null, "Comment to write cannot be null");

        logger.debug("Request for writing comment");
        String thisMethodSignature = "add-comment-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, comment);
    }
    public Comment readCommentByIdLib(String commentid){
        Validate.isTrue(commentid != null, "Commentid to read cannot be null");

        logger.debug("Request for reading commentid");
        String thisMethodSignature = "read-comment-by-id-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?commentid=" + commentid;
        return HttpClient.get(new GenericType<ResponseBean<Comment>>(){}, request);
    }
    public List<Comment> getAllCommentsByPostIdLib(String postid){
        Validate.isTrue(postid != null, "Postid to read cannot be null");

        logger.debug("Request for reading comments of post");
        String thisMethodSignature = "get-comments-by-post-id-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + postid;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Comment>>>(){}, request);
    }
    public List<Comment> getRecentCommentsByUserAndDateLib(final String userid,
                                                           final long timeInMillis){

        Validate.isTrue(userid != null, "username to read cannot be null");

        logger.debug("Request for reading recent comments of user");
        String thisMethodSignature = "get-recent-comments-by-user-and-date-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?time=" + timeInMillis + "&username=" + userid;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Comment>>>(){}, request);
    }

    public Boolean editCommentLib(Comment comment){
        Validate.isTrue(comment != null, "Comment to write cannot be null");

        logger.debug("Request for writing comment");
        String thisMethodSignature = "edit-comment-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, comment);
    }

    public boolean deleteCommentLib(String commentid, String postid){
        Validate.isTrue(commentid != null, "Comment to write cannot be null");

        logger.debug("Request for del comment");
        String thisMethodSignature = "delete-comment-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?commentid=" + commentid + "&postid=" + postid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, postid);
    }

    //Hashtags
    public boolean saveHashTagsLib(String feedid, String vreid, List<String> hashtags) {
        Validate.isTrue(hashtags != null, "hashtags to write cannot be null");

        logger.debug("Request for saving hashtags");
        String thisMethodSignature = "save-hashtag-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?feedid=" + feedid + "&vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, hashtags);
    }

    public boolean deleteHashTagsLib(String feedid, String vreid, List<String> hashtags) {
        Validate.isTrue(hashtags != null, "hashtags to delete cannot be null");

        logger.debug("Request for deleting hashtags");
        String thisMethodSignature = "delete-hashtag-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?feedid=" + feedid + "&vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, hashtags);
    }

    public boolean saveHashTagsCommentLib(String commentId, String vreid, List<String> hashtags){
        Validate.isTrue(hashtags != null, "hashtags to comment cannot be null");

        logger.debug("Request for saving hashtags comment");
        String thisMethodSignature = "save-hashtag-comment-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?commentid=" + commentId + "&vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, hashtags);
    }

    public boolean deleteHashTagsCommentLib(String commentId, String vreid, List<String> hashtags) {
        Validate.isTrue(hashtags != null, "hashtags to comment delete cannot be null");

        logger.debug("Request for deleting hashtags comment");
        String thisMethodSignature = "delete-hashtag-comment-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?commentid=" + commentId + "&vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, hashtags);
    }

    public Map<String, Integer> getVREHashtagsWithOccurrenceLib(String vreid) {
        logger.debug("Request for vre hashtags occurrences");
        String thisMethodSignature = "get-vre-hashtags-occurrences-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid;
        return HttpClient.get(new GenericType<ResponseBean<HashMap<String, Integer>>>(){}, request);
    }

    public Map<String, Integer> getVREHashtagsWithOccurrenceFilteredByTimeLib(String vreid, long timestamp){
        logger.debug("Request for vre hashtags occurrences with time");
        String thisMethodSignature = "get-vre-hashtags-occurrences-time-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&time=" + timestamp;
        return HttpClient.get(new GenericType<ResponseBean<HashMap<String, Integer>>>(){}, request);
    }


    public List<Post> getVREPostsByHashtagLib(String vreid, String hashtag){
        hashtag = hashtag.substring(1);
        logger.debug("Request for vre of hashtags");
        String thisMethodSignature = "get-vre-post-by-hashtag-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&hashtag=" + hashtag;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }

    //Invites
    public String isExistingInviteLib(String vreid, String email){
        Validate.isTrue(email != null, "email to invite cannot be null");

        logger.debug("Request for inviting to vre");
        String thisMethodSignature = "is-existing-invite-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&email=" + email;
        return HttpClient.get(new GenericType<ResponseBean<String>>(){}, request);
    }
    public InviteOperationResult saveInviteLib(Invite invite){
        Validate.isTrue(invite != null, "likeid to unlike cannot be null");

        logger.debug("Request for saving invite");
        String thisMethodSignature = "save-invite-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<InviteOperationResult>>(){}, request, invite);
    }
    public Invite readInviteLib(String inviteid){
        Validate.isTrue(inviteid != null, "inviteid to read likes cannot be null");

        logger.debug("Request for reading invite");
        String thisMethodSignature = "read-invite-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?inviteid=" + inviteid;
        return HttpClient.get(new GenericType<ResponseBean<Invite>>(){}, request);
    }
    public boolean setInviteStatusLib(String vreid, String email, InviteStatus status){
        Validate.isTrue(status != null, "status to set cannot be null");

        logger.debug("Request for setting invite status");
        String thisMethodSignature = "set-invite-status-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&email=" + email;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, status);
    }

    public List<Invite> getInvitedEmailsByVRELib(String vreid, InviteStatus... status){
        Validate.isTrue(status != null, "status to get cannot be null");

        logger.debug("Request for getting email invites");
        String thisMethodSignature = "get-invited-email-by-vre-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<ArrayList<Invite>>>(){}, request, status);
    }

    //Likes
    public boolean likeLib(Like like){
        Validate.isTrue(like != null, "like to write cannot be null");

        logger.debug("Request for liking post");
        String thisMethodSignature = "like-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, like);
    }
    public boolean unlikeLib(String userid, String likeid, String feedid){
        Validate.isTrue(likeid != null, "likeid to unlike cannot be null");

        logger.debug("Request for unliking post");
        String thisMethodSignature = "unlike-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid + "&likeid=" + likeid + "&feedid=" + feedid ;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, likeid);
    }
    public List<String>getAllLikedPostIdsByUserLib(String userid){
        Validate.isTrue(userid != null, "userid to read likes cannot be null");

        logger.debug("Request for reading likes of user");
        String thisMethodSignature = "get-all-liked-posts-by-user-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);
    }
    public List<Like>getAllLikesByPostLib(String postid){
        Validate.isTrue(postid != null, "postid to read likes cannot be null");

        logger.debug("Request for reading likes of post");
        String thisMethodSignature = "get-all-likes-by-post-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + postid;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Like>>>(){}, request);
    }

    //Notifications
    public boolean saveNotificationLib(Notification n){
        Validate.isTrue(n != null, "Notification cannot be null");
        logger.debug("Request for saving notification");
        String thisMethodSignature = "save-notification-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, n);
    }
    public Notification readNotificationLib(String notificationid){
        logger.debug("Request for getting notification by id");
        String thisMethodSignature = "read-notification-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?notid=" + notificationid;
        return HttpClient.get(new GenericType<ResponseBean<Notification>>(){}, request);
    }
    public boolean setNotificationReadLib(String notificationid){
        Validate.isTrue(notificationid != null, "Notificationid cannot be null");
        logger.debug("Request for setting notification read");
        String thisMethodSignature = "set-notification-read-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?notid=" + notificationid;;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, notificationid);
    }

    public List<Notification> getAllNotificationByUserLib(String userid, int limit) {
        logger.debug("Request for getting notification by user");
        String thisMethodSignature = "get-all-notifications-user";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid + "&limit=" + limit;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Notification>>>(){}, request);
    }

    public List<Notification> getUnreadNotificationsByUserLib(String userid) {
        logger.debug("Request for getting unread notification by user");
        String thisMethodSignature = "get-unread-notifications-user";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Notification>>>(){}, request);
    }

    public List<Notification> getRangeNotificationsByUserLib(String userid,int from, int quantity) {
        logger.debug("Request for getting range notification by user");
        String thisMethodSignature = "get-range-notifications-user";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid + "&from=" + from + "&quantity=" + quantity;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Notification>>>(){}, request);
    }

    public boolean setAllNotificationReadByUserLib(String userid) {
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for setting all notification read");
        String thisMethodSignature = "set-all-notification-read-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, userid);
    }
    public boolean checkUnreadNotificationsLib(String userid) {
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for check unread notifications");
        String thisMethodSignature = "check-unread-notification-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, userid);
    }
    public boolean checkUnreadMessagesNotificationsLib(String userid) {
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for check unread messages notifications");
        String thisMethodSignature = "check-unread-messages-notification-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, userid);

    }

    public List<NotificationChannelType> getUserNotificationChannelsLib(String userid, NotificationType notificationType) {
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for get user notification channels");
        String thisMethodSignature = "get-user-notification-channels-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid + "&type=" + notificationType.toString();
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<NotificationChannelType>>>(){}, request);

    }

    public boolean setUserNotificationPreferencesLib(String userid, Map<NotificationType, NotificationChannelType[]> enabledChannels){
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for set user notification preferences");
        String thisMethodSignature = "set-notification-preference-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;
        for(NotificationType notificationType: enabledChannels.keySet()){
            logger.info("Type: " + notificationType.toString());
            for(NotificationChannelType channelType: enabledChannels.get(notificationType)){
                logger.info(channelType.toString());
            }
        }
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, enabledChannels);

    }

    public Map<NotificationType, NotificationChannelType[]> getUserNotificationPreferencesLib(String userid){
        Validate.isTrue(userid != null, "userid cannot be null");
        logger.debug("Request for getting notification preferences");
        String thisMethodSignature = "get-notification-preference-lib";
        String request =  getServiceEndpoint()  + thisMethodSignature + "?userid=" + userid;;
        return HttpClient.get(new GenericType<ResponseBean<HashMap<NotificationType, NotificationChannelType[]>>>(){}, request);
    }


    public boolean saveUserPostLib(Post post){
        Validate.isTrue(post != null, "Post to write cannot be null");

        logger.debug("Request for save user post");
        String thisMethodSignature = "save-user-post-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, post);
    } //bool

    public boolean saveAttachmentEntryLib(String postkey, Attachment attachment){
        Validate.isTrue(attachment != null, "Attachment to write cannot be null");

        logger.debug("Request for save attachment");
        String thisMethodSignature = "save-attachment-lib" + "?postid=" + postkey;
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, attachment);
    }

    public boolean saveUserPostLib(Post post, List<Attachment> attachments ){
        Validate.isTrue(post != null, "Post to write cannot be null");

        if (attachments != null && !attachments.isEmpty())
            post.setMultiFileUpload(true);
        boolean savePostResult = saveUserPostLib(post);
        if (savePostResult) {
            logger.info("Post has been saved");
            String postkey = post.getKey();
            for (Attachment attachment : attachments) {
                boolean attachSaveResult = saveAttachmentEntryLib(postkey, attachment);
                if (!attachSaveResult)
                    logger.info("Some of the attachments failed to be saved: " + attachment.getName());
            }
            return true;
        }
        else return false;
    }

    public boolean saveAppPostLib(Post post, List<Attachment> attachments) {
        Validate.isTrue(post != null, "Post to write cannot be null");

        if (attachments != null && !attachments.isEmpty())
            post.setMultiFileUpload(true);
        boolean savePostResult = saveAppPostLib(post);
        if (savePostResult) {
            logger.info("Post has been saved");
            String postkey = post.getKey();
            for (Attachment attachment : attachments) {
                boolean attachSaveResult = saveAttachmentEntryLib(postkey, attachment);
                if (!attachSaveResult)
                    logger.info("Some of the attachments failed to be saved: " + attachment.getName());
            }
            return true;
        }
        else return false;

    }

    public boolean saveAppPostLib(Post post){
        Validate.isTrue(post != null, "Post to write cannot be null");

        logger.debug("Request for saving application post");
        String thisMethodSignature = "save-app-post-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, post);
    }   //bool

    public boolean savePostToVRETimelineLib(String postkey, String vreid){
        Validate.isTrue(postkey != null, "Post to write cannot be null");

        logger.debug("Request for writing post to vretimeline");
        String thisMethodSignature = "save-post-to-vretimeline-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + postkey + "&vreid=" + vreid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, postkey);
    }  //bool

    public Post readPostLib(String postid){
        logger.debug("Request for getting post with id");
        String thisMethodSignature = "read-post-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + postid;
        return HttpClient.get(new GenericType<ResponseBean<Post>>(){}, request);
    }    //post

    public List<Post> getRecentPostsByUserAndDateLib(String userid, long timeinmillis){
        logger.debug("Request for getting recent posts by user");
        String thisMethodSignature = "get-recent-posts-by-user-date-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid + "&time=" + timeinmillis;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }  //list<post>

    public boolean deletePostLib(String postid){
        Validate.isTrue(postid != null, "feed to delete cannot be null");

        logger.debug("Request for delete user post");
        String thisMethodSignature = "delete-post-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + postid;
        return HttpClient.post(new GenericType<ResponseBean<Boolean>>(){}, request, postid);
    }   //bool

    public List<Post> getAllPostsByUserLib(String userid) {
        logger.debug("Request for getting all posts by user");
        String thisMethodSignature = "get-all-posts-by-user-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }

    public List<Post> getAllPostsByAppLib(String appid) {
        logger.debug("Request for getting all posts by app");
        String thisMethodSignature = "get-all-posts-by-app-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?appid=" + appid ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

    }

    public List<Post> getRecentCommentedPostsByUserAndDateLib(String userid,
                                                              long timeInMillis) {
        logger.debug("Request for getting recent commented posts by user and date");
        String thisMethodSignature = "get-recent-commented-posts-by-user-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid + "&time=" + timeInMillis;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

    }

    public List<Post> getAllPortalPrivacyLevelPostsLib(){
        logger.debug("Request for getting portal privacy level posts");
        String thisMethodSignature = "get-portal-privacy-level-posts-lib";
        String request =  getServiceEndpoint() + thisMethodSignature;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }   //list<post>

    public List<Post> getRecentPostsByUserLib(String userid, int quantity) {
        logger.debug("Request for getting recent posts by user");
        String thisMethodSignature = "get-recent-posts-by-user-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?userid=" + userid + "&quantity=" + quantity ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }

    public List<Post> getAllPostsByVRELib(String vreid) {
        logger.debug("Request for getting all posts by vre");
        String thisMethodSignature = "get-all-posts-by-vre-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);
    }

    public List<Post> getRecentPostsByVRELib(String vreid, int quantity) {
        logger.debug("Request for getting recent posts by vre");
        String thisMethodSignature = "get-recent-posts-by-vre-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&quantity=" + quantity ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

    }

    public RangePosts getRecentPostsByVREAndRangeLib(String vreid, int from, int quantity) {
        logger.debug("Request for getting recent posts by vre and range");
        String thisMethodSignature = "get-recent-posts-by-vre-range-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?vreid=" + vreid + "&from=" + from + "&quantity=" + quantity;
        return HttpClient.get(new GenericType<ResponseBean<RangePosts>>(){}, request);
    }

    public List<Attachment> getAttachmentsByFeedIdLib(String feedId) {
        logger.debug("Request for getting attachment by postid");
        String thisMethodSignature = "get-attachment-by-postid-lib";
        String request =  getServiceEndpoint() + thisMethodSignature + "?postid=" + feedId ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<Attachment>>>(){}, request);
    }

    public List<String> getAllVREIdsLib(){
        logger.debug("Request for getting all vre ids");
        String thisMethodSignature = "get-all-vre-ids-lib";
        String request =  getServiceEndpoint() + thisMethodSignature ;
        return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);
    }
}
