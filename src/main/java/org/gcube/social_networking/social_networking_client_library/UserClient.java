package org.gcube.social_networking.social_networking_client_library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.UserProfileExtended;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Users client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class UserClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/users/";
	private static Logger logger = LoggerFactory.getLogger(UserClient.class);


	public UserClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}

	/**
	 * Get attribute value
	 * @return
	 */
	public String getCustomAttribute(String attributeKey){

		Validate.isTrue(attributeKey != null, "attribute key cannot be null");

		logger.debug("Request for attribute value");
		String thisMethodSignature = "get-custom-attribute";
		String request =  getServiceEndpoint()  + thisMethodSignature + "?attribute=" + attributeKey;
		return HttpClient.get(new GenericType<ResponseBean<String>>(){}, request);

	}

	/**
	 * Get fullname
	 * @return user's fullname
	 */
	public String getFullName(){

		logger.debug("Request for fullname");
		String thisMethodSignature = "get-fullname";
		String request =  getServiceEndpoint()  + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<String>>(){}, request);

	}

	/**
	 * Get email
	 * @return users'email
	 */
	public String getEmail(){

		logger.debug("Request for email");
		String thisMethodSignature = "get-email";
		String request =  getServiceEndpoint()  + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<String>>(){}, request);

	}

	/**
	 * Get profile
	 * @return
	 */
	public UserProfileExtended getProfile(){

		logger.debug("Request for getting profile");
		String thisMethodSignature = "get-profile";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<UserProfileExtended>>(){}, request);

	}

	/**
	 * Get all usernames in this context
	 * @return
	 */
	public List<String> getAllUsernamesContext(){

		logger.debug("Request for getting usernames in this context");
		String thisMethodSignature = "get-all-usernames";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);

	}

	/**
	 * Get all usernames and fullnames in this context
	 * @return
	 */
	public Map<String, String> getAllUsernamesFullnamesContext(){

		logger.debug("Request for getting usernames and fullnames in this context");
		String thisMethodSignature = "get-all-fullnames-and-usernames";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<HashMap<String, String>>>(){}, request);

	}

	/**
	 * Get usernames by global role
	 * @return
	 */
	public List<String> getAllUsernamesByGlobalRole(String globalRole){

		Validate.isTrue(globalRole != null, "role cannot be null");

		logger.debug("Request for getting usernames with global role");
		String thisMethodSignature = "get-usernames-by-global-role";
		String request =  getServiceEndpoint() + thisMethodSignature + "?role-name=" + globalRole;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);

	}

	/**
	 * Get usernames by role
	 * @deprecated use #getAllUsernamesByRole(String rolename) instead
	 * @return the usernames having such role
	 */
	@Deprecated
	public List<String> getAllUsernamesByLocalRole(String localRole){

		Validate.isTrue(localRole != null, "role cannot be null");

		logger.debug("Request for getting usernames by role role");
		String thisMethodSignature = "get-usernames-by-role";
		String request =  getServiceEndpoint() + thisMethodSignature + "?role-name=" + localRole;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);

	}
	/**
	 * Get usernames by role, the environment (i.e. VRE/VLab) is inferred from the token
	 * @return the usernames having such role
	 */
	public Set<String>getAllUsernamesByRole(String rolename){
		Validate.isTrue(rolename != null, "role cannot be null");

		logger.debug("Request for getting usernames with global role");
		String thisMethodSignature = "get-usernames-by-role";
		String request =  getServiceEndpoint() + thisMethodSignature + "?role-name=" + rolename;
		ArrayList<String> sourceList = HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);
		Set<String> usernames = new HashSet<>(sourceList);
		return usernames;
	}

	/**
	 * Check if user exists
	 * @return
	 */
	public Boolean userExists(String username){

		Validate.isTrue(username != null, "username cannot be null");

		logger.debug("Request for getting usernames with global role");
		String thisMethodSignature = "user-exists";
		String request =  getServiceEndpoint() + thisMethodSignature + "?username=" + username;
		return HttpClient.get(new GenericType<ResponseBean<Boolean>>(){}, request);

	}

}
