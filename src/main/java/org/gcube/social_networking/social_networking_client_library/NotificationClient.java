package org.gcube.social_networking.social_networking_client_library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.portal.databook.shared.Notification;
import org.gcube.portal.databook.shared.NotificationChannelType;
import org.gcube.portal.databook.shared.NotificationType;
import org.gcube.portal.databook.shared.ex.ColumnNameNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationChannelTypeNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationIDNotFoundException;
import org.gcube.portal.databook.shared.ex.NotificationTypeNotFoundException;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.JobNotificationBean;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEvent;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.WorkspaceEvent;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.WorkspaceEventType;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Notifications client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * @author Ahmed Ibrahim at ISTI-CNR (ahmed.ibrahim@isti.cnr.it)
 */
public class NotificationClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/notifications/";
	private static Logger logger = LoggerFactory.getLogger(NotificationClient.class);
	
	
	public NotificationClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}
	
	/**
	 * Get range notifications
	 * @param from greater or equal to one
	 * @param quantity
	 * @return
	 */
	public List<Notification> getNotifications(int from, int quantity){
		
		Validate.isTrue(from >= 1, "From cannot be negative");
		Validate.isTrue(quantity >= 0, "Quantity cannot be negative");
		
		logger.debug("Request for getting notifications");
		String thisMethodSignature = "get-range-notifications";
		String request =  getServiceEndpoint()  + thisMethodSignature + "?from=" + from + "&quantity=" +quantity;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Notification>>>(){}, request);
		
	}
	
	/**
	 * Notify job status
	 * @param notification
	 * @return
	 */
	public void sendJobNotification(JobNotificationBean notification){
		
		Validate.isTrue(notification != null, "Notification cannot be null");
		logger.debug("Request for getting notifications");
		String thisMethodSignature = "notify-job-status";
		String request =  getServiceEndpoint()  + thisMethodSignature;
		HttpClient.post(new GenericType<ResponseBean<String>>(){}, request, notification);
		
	}
	
	/**
	 * Notify a catalogue event, types available see {@link WorkspaceEventType}
	 * @param event an instance of {@link WorkspaceEvent}
	 */
	public void sendWorkspaceEvent(WorkspaceEvent event) {		
		Validate.isTrue(event != null, "WorkspaceEvent cannot be null");
		logger.debug("Request for sending workdspace notifications");
		String thisMethodSignature = "workspace";
		String request =  getServiceEndpoint()  + thisMethodSignature;
		HttpClient.post(new GenericType<ResponseBean<String>>(){}, request, event);
		
	}
	
	/**
	 * Notify a catalogue event, types availeble see {@link CatalogueEventType}
	 * @param event an instance of {@link CatalogueEvent}
	 */
	public void sendCatalogueEvent(CatalogueEvent event) {		
		Validate.isTrue(event != null, "CatalogueEvent cannot be null");
		logger.debug("Request for sending CatalogueEvent notifications");
		String thisMethodSignature = "catalogue";
		String request =  getServiceEndpoint()  + thisMethodSignature;
		HttpClient.post(new GenericType<ResponseBean<String>>(){}, request, event);
		
	}
}
