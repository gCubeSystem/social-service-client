package org.gcube.social_networking.social_networking_client_library;

import javax.ws.rs.core.GenericType;

import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.UserProfile;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * People client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class PeopleClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/people/";
	private static Logger logger = LoggerFactory.getLogger(NotificationClient.class);


	public PeopleClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}
	
	/**
	 * Get profile
	 * @return user's profile
	 */
	public UserProfile getProfile(){
		
		logger.debug("Request for getting profile");
		String thisMethodSignature = "profile";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<UserProfile>>(){}, request);
	}

}
