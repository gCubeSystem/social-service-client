package org.gcube.social_networking.social_networking_client_library.utils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.social_networking.model.providers.CustomObjectMapper;
import org.gcube.social_networking.social_networking_client_library.filter.AuthorizationFilter;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jersey client with post and get methods
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class HttpClient{

	private static Logger logger = LoggerFactory.getLogger(HttpClient.class);

	/**
	 * Executes a get request
	 * @param returnType
	 * @param requestUrl
	 * @return
	 */
	public static <R> R get(GenericType<ResponseBean<R>> returnType, String requestUrl){

		logger.debug("Executing get request at url " + requestUrl);

		ClientConfig cc = new ClientConfig().register(new JacksonFeature()).register(new AuthorizationFilter()).register(new CustomObjectMapper());
		Client client = ClientBuilder.newClient(cc);
		WebTarget webResourceGet = client.target(requestUrl);
		Response response = webResourceGet.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();

		if(response == null)
			throw new WebApplicationException("Response returned by the service is null");

		if(response.getStatus() >= 300)
			throw new WebApplicationException("There was an error at server side: " + response.getStatusInfo());

		ResponseBean<R> result = response.readEntity(returnType);

		if (!result.isSuccess())
			throw new WebApplicationException(
					"Error message is " + result.getMessage());

		return result.getResult();
	}

	/**
	 * Executes a post request
	 * @param returnType
	 * @param requestUrl
	 * @param obj
	 * @return
	 */
	public static <R> R post(GenericType<ResponseBean<R>> returnType, String requestUrl, Object obj){

		logger.debug("Executing post request at url " + requestUrl);

		ClientConfig cc = new ClientConfig().register(new JacksonFeature()).register(new AuthorizationFilter()).register(new CustomObjectMapper());;
		Client client = ClientBuilder.newClient(cc);
		WebTarget webResourceGet = client.target(requestUrl);
		logger.debug("Entity looks like " + Entity.json(obj));
		Response response = webResourceGet.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(Entity.json(obj));

		if(response == null)
			throw new WebApplicationException("Response returned by the service is null");

		if(response.getStatus() >= 300)
			throw new WebApplicationException("There was an error at server side: " + response.getStatusInfo());

		ResponseBean<R> result = response.readEntity(returnType);

		if (!result.isSuccess())
			throw new WebApplicationException(
					"Error message is " + result.getMessage());

		return result.getResult();
	}
	
	/**
	 * Executes a post request
	 * @param returnType
	 * @param requestUrl
	 * @param obj
	 * @return
	 */
	public static <R> R postFormData(GenericType<ResponseBean<R>> returnType, String requestUrl, Form form){

		logger.debug("Executing post request at url " + requestUrl);

		ClientConfig cc = new ClientConfig().register(new JacksonFeature()).register(new AuthorizationFilter()).register(new CustomObjectMapper());;
		Client client = ClientBuilder.newClient(cc);
		WebTarget webResourceGet = client.target(requestUrl);
		Builder basicRequest = webResourceGet.request();  
		Response response = basicRequest.post(Entity.form(form), Response.class);

		if(response == null)
			throw new WebApplicationException("Response returned by the service is null");

		if(response.getStatus() >= 300)
			throw new WebApplicationException("There was an error at server side: " + response.getStatusInfo());

		ResponseBean<R> result = response.readEntity(returnType);

		if (!result.isSuccess())
			throw new WebApplicationException(
					"Error message is " + result.getMessage());

		return result.getResult();
	}
}
