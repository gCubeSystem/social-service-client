/**
 *
 */
package org.gcube.social_networking.social_networking_client_library.utils;

import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.social_networking.social_networking_client_library.exceptions.NoSocialServiceAvailable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Discovers the social networking service endpoint in a given context
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class ServiceDiscoverer {

	private static final String resource = "jersey-servlet";
	private static final String serviceName = "SocialNetworking";
	private static final String serviceClass = "Portal";

	private static Logger logger = LoggerFactory.getLogger(ServiceDiscoverer.class);
	private String entryPoint;

	/**
	 * Discover the entry point for the service
	 * @throws Exception
	 */
	public ServiceDiscoverer() throws Exception {

		String currentScope = ScopeProvider.instance.get();
		
		if(currentScope == null || currentScope.isEmpty())
			throw new RuntimeException("Scope is not set");

		try{

			logger.debug("set scope "+currentScope);

			SimpleQuery query = queryFor(GCoreEndpoint.class);
			query.addCondition(String.format("$resource/Profile/ServiceClass/text() eq '%s'",serviceClass));
			query.addCondition("$resource/Profile/DeploymentData/Status/text() eq 'ready'");
			query.addCondition(String.format("$resource/Profile/ServiceName/text() eq '%s'",serviceName));
			query.setResult("$resource/Profile/AccessPoint/RunningInstanceInterfaces//Endpoint[@EntryName/string() eq \""+resource+"\"]/text()");

			logger.debug("submitting quey "+query.toString());

			DiscoveryClient<String> client = client();
			List<String> endpoints = client.submit(query);
			if (endpoints == null || endpoints.isEmpty()) 
				throw new NoSocialServiceAvailable("Cannot retrieve the GCoreEndpoint serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+currentScope);


			this.entryPoint = endpoints.get(0);
			if(entryPoint==null)
				throw new NoSocialServiceAvailable("Endpoint:"+resource+", is null for serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+currentScope);

			logger.info("found entryPoint "+entryPoint+" for social service: "+resource);

		}catch(Exception e){
			String error = "An error occurred during GCoreEndpoint discovery, serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+currentScope +".";
			logger.error(error, e);
			throw new Exception(error);
		}
	}

	/**
	 * @return the ResourceEntyName
	 */
	public String getEntryPoint() {

		return entryPoint;
	}
}
