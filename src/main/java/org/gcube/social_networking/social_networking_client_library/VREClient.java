package org.gcube.social_networking.social_networking_client_library;

import java.util.List;

import javax.ws.rs.core.GenericType;

import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.MyVRE;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Vres client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class VREClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/vres/";
	private static Logger logger = LoggerFactory.getLogger(VREClient.class);


	public VREClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}
	
	/**
	 * Get my Vres
	 * @return my vres
	 */
	public List<MyVRE> getMyVRES(boolean retrieveManagers){
		
		logger.debug("Request for writing new message");
		String thisMethodSignature = "get-my-vres";
		String request =  getServiceEndpoint() + thisMethodSignature + "?" + "getManagers=" + retrieveManagers;
		return HttpClient.get(new GenericType<ResponseBean<List<MyVRE>>>(){}, request);
		
	}

}
