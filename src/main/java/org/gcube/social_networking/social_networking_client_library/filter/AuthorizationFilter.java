package org.gcube.social_networking.social_networking_client_library.filter;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Authorization filter for the jersey client.
 */
public class AuthorizationFilter implements ClientRequestFilter{
	private final static String AUTH_TOKEN_PARAMETER = "Authorization";
	private final static String LEGACY_AUTH_TOKEN_PARAMETER = "gcube-token";
	private static Logger logger = LoggerFactory.getLogger(AuthorizationFilter.class);

	@Override
	public void filter(ClientRequestContext original) throws IOException {
		logger.debug("Adding token to the request " + original.getUri());
		String token = AccessTokenProvider.instance.get();
		String legacyToken = SecurityTokenProvider.instance.get();
		if (token != null)
			original.getHeaders().add(AUTH_TOKEN_PARAMETER, " Bearer " + token);
		if (legacyToken != null)
			original.getHeaders().add(LEGACY_AUTH_TOKEN_PARAMETER, legacyToken);

	}
}
