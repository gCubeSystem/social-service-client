package org.gcube.social_networking.social_networking_client_library;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.ApplicationId;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tokens client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class TokenClient extends BaseClient{
	
	private static final String SUB_SERVICE_PATH = "2/tokens/";
	private static Logger logger = LoggerFactory.getLogger(TokenClient.class);


	public TokenClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}
	
	/**
	 * Generate application token
	 * @return generated token
	 */
	public String generateApplicationToken(ApplicationId appId){
		
		Validate.isTrue(appId != null, "application id cannot be null");
		
		logger.debug("Request for writing new message");
		String thisMethodSignature = "generate-application-token";
		String request =  getServiceEndpoint() + thisMethodSignature + "?";
		return HttpClient.post(new GenericType<ResponseBean<String>>(){}, request, appId);
		
	}

}
