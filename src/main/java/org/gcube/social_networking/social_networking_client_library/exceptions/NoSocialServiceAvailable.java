package org.gcube.social_networking.social_networking_client_library.exceptions;

/**
 * No social service available in the infrastructure.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class NoSocialServiceAvailable extends Exception {

	public NoSocialServiceAvailable(String errorMsg) {
		super(errorMsg);
	}

	private static final long serialVersionUID = 5927949225149691036L;

}
