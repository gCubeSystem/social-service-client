package org.gcube.social_networking.social_networking_client_library;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.portal.databook.shared.*;

import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.beans.PostInputBean;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Posts client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * @author Ahmed Ibrahim at ISTI-CNR (ahmed.ibrahim@isti.cnr.it)
 */
public class PostClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/posts/";
	private static Logger logger = LoggerFactory.getLogger(PostClient.class);


	public PostClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}

	/**
	 * Get posts since date
	 * @return
	 */
	public List<Post> getUserPostsSinceDate(long timeInMillis){

		Validate.isTrue(timeInMillis >= 0, "time cannot be negative");

		logger.debug("Request for getting posts");
		String thisMethodSignature = "get-posts-user-since";
		String request =  getServiceEndpoint() + thisMethodSignature + "?time=" + timeInMillis;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Get all posts
	 * @return
	 */
	public List<Post> getAllUserPosts(){

		logger.debug("Request for getting posts");
		String thisMethodSignature = "get-posts-user";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Get posts quantity
	 * @return
	 */
	public List<Post> getUserPostsQuantity(int quantity){

		Validate.isTrue(quantity >= 0, "quantity cannot be negative");

		logger.debug("Request for getting posts");
		String thisMethodSignature = "get-posts-user-quantity";
		String request =  getServiceEndpoint() + thisMethodSignature + "?quantity=" + quantity;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Write post
	 * @return
	 */
	public Post writeUserPost(PostInputBean toWrite){

		Validate.isTrue(toWrite != null, "Post to write cannot be null");

		logger.debug("Request for writing post");
		String thisMethodSignature = "write-post-user";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.post(new GenericType<ResponseBean<Post>>(){}, request, toWrite);

	}

	/**
	 * Get posts application (token set must belong to the application)
	 * @return
	 */
	public List<Post> getAllApplicationPosts(){

		logger.debug("Request for getting posts");
		String thisMethodSignature = "get-posts-app";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Write post application (token set must belong to the application)
	 * @return
	 */
	public Post writeApplicationPost(PostInputBean toWrite){

		Validate.isTrue(toWrite != null, "Post to write cannot be null");

		logger.debug("Request for writing application post");
		String thisMethodSignature = "write-post-app";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.post(new GenericType<ResponseBean<Post>>(){}, request, toWrite);

	}

	/**
	 * @deprecated use  List<Post> getVREPosts()
	 * Get posts vre
	 * @return
	 */
	public List<Feed> getPostsVRE(){

		logger.debug("Request for getting posts vre");
		String thisMethodSignature = "get-posts-vre";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Feed>>>(){}, request);

	}
	/**
	 * Get posts vre
	 * @return
	 */
	public List<Post> getVREPosts(){

		logger.debug("Request for getting posts vre");
		String thisMethodSignature = "get-posts-vre";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Get posts hashtag
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public List<Post> getHashtagPosts(String hashtag) throws UnsupportedEncodingException{

		Validate.isTrue(hashtag != null, "hashtag cannot be null");

		logger.debug("Request for getting posts with hashtag " + hashtag);
		String thisMethodSignature = "get-posts-by-hashtag";
		String request =  getServiceEndpoint() + thisMethodSignature + "?hashtag=" + URLEncoder.encode(hashtag, "UTF-8");
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Get liked posts 
	 * @return
	 */
	public List<Post> getUserLikedPost(){

		logger.debug("Request for getting posts liked");
		String thisMethodSignature = "get-liked-posts";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<Post>>>(){}, request);

	}

	/**
	 * Get liked posts ids
	 * @return
	 */
	public List<String> getUserLikedPostIds(){

		logger.debug("Request for getting posts liked");
		String thisMethodSignature = "get-id-liked-posts";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<String>>>(){}, request);

	}
}
