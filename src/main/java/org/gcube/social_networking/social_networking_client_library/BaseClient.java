package org.gcube.social_networking.social_networking_client_library;

import org.gcube.social_networking.social_networking_client_library.utils.ServiceDiscoverer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base client service.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public abstract class BaseClient {

		private static Logger logger = LoggerFactory.getLogger(BaseClient.class);
		private String serviceEndpoint;

		public BaseClient(String subPath) throws Exception {
			ServiceDiscoverer discoverer = new ServiceDiscoverer();
			serviceEndpoint = discoverer.getEntryPoint();
			logger.debug("Discovering service...");
			serviceEndpoint = serviceEndpoint.endsWith("/") ? serviceEndpoint + subPath : serviceEndpoint + "/" + subPath;
			logger.info("Discovered service " + serviceEndpoint);
		}

		public String getServiceEndpoint() {
			return serviceEndpoint;
		}
}
