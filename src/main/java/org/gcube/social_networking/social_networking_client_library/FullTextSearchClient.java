package org.gcube.social_networking.social_networking_client_library;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.portal.databook.shared.EnhancedFeed;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Client for full text search over social content.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class FullTextSearchClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/full-text-search/";
	private static Logger logger = LoggerFactory.getLogger(FullTextSearchClient.class);

	public FullTextSearchClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}

	/**
	 * Perform a search query over social data
	 * @param query
	 * @param from a value greater or equal to zero
	 * @param quantity a value greater than zero
	 * @return a list of matching posts
	 * @throws UnsupportedEncodingException 
	 */
	public ArrayList<EnhancedFeed> search(String query, int from, int quantity) throws UnsupportedEncodingException{

		Validate.isTrue(query != null, "Query cannot be null");
		Validate.isTrue(from >= 0, "From needs to be greater or equal to zero");
		Validate.isTrue(quantity >= 0, "Quantity needs to be greater or equal to zero");

		logger.debug("Request for query " + query + " and from " + from + " and quantity is " + quantity);
		String thisMethodSignature = "search-by-query";
		String request = getServiceEndpoint() + thisMethodSignature + "?" + "query=" + URLEncoder.encode(query, "UTF-8") + "&from=" + from + "&quantity=" + quantity;
		return HttpClient.get(new GenericType<ResponseBean<ArrayList<EnhancedFeed>>>(){}, request);

	}

}
