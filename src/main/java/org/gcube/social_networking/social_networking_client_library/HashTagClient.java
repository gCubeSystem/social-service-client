package org.gcube.social_networking.social_networking_client_library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericType;

import org.apache.commons.lang.Validate;
import org.gcube.portal.databook.shared.Feed;
import org.gcube.portal.databook.shared.Post;
import org.gcube.portal.databook.shared.ex.*;
import org.gcube.social_networking.social_networking_client_library.utils.HttpClient;
import org.gcube.social_networking.socialnetworking.model.output.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * HashTags client.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * @author Ahmed Ibrahim at ISTI-CNR (ahmed.ibrahim@isti.cnr.it)
 */
public class HashTagClient extends BaseClient{

	private static final String SUB_SERVICE_PATH = "2/hashtags/";
	private static Logger logger = LoggerFactory.getLogger(HashTagClient.class);


	public HashTagClient() throws Exception {
		super(SUB_SERVICE_PATH);
	}

	/**
	 * Get hashtags and their count
	 * @return a map of type hashtag -> number
	 */
	public Map<String, Integer> getHashtagsCount(){
		logger.debug("Request for hastags");
		String thisMethodSignature = "get-hashtags-and-occurrences";
		String request =  getServiceEndpoint() + thisMethodSignature;
		return HttpClient.get(new GenericType<ResponseBean<HashMap<String, Integer>>>(){}, request);
	}
}
