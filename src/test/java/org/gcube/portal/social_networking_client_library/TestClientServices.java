package org.gcube.portal.social_networking_client_library;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portal.databook.shared.EnhancedFeed;
import org.gcube.portal.databook.shared.Notification;
import org.gcube.portal.databook.shared.Post;
import org.gcube.social_networking.social_networking_client_library.FullTextSearchClient;
import org.gcube.social_networking.social_networking_client_library.HashTagClient;
import org.gcube.social_networking.social_networking_client_library.MessageClient;
import org.gcube.social_networking.social_networking_client_library.NotificationClient;
import org.gcube.social_networking.social_networking_client_library.PeopleClient;
import org.gcube.social_networking.social_networking_client_library.PostClient;
import org.gcube.social_networking.social_networking_client_library.TokenClient;
import org.gcube.social_networking.social_networking_client_library.UserClient;
import org.gcube.social_networking.social_networking_client_library.VREClient;
import org.gcube.social_networking.socialnetworking.model.beans.ApplicationId;
import org.gcube.social_networking.socialnetworking.model.beans.JobNotificationBean;
import org.gcube.social_networking.socialnetworking.model.beans.JobStatusModelType;
import org.gcube.social_networking.socialnetworking.model.beans.MessageInbox;
import org.gcube.social_networking.socialnetworking.model.beans.MessageInputBean;
import org.gcube.social_networking.socialnetworking.model.beans.MyVRE;
import org.gcube.social_networking.socialnetworking.model.beans.PostInputBean;
import org.gcube.social_networking.socialnetworking.model.beans.Recipient;
import org.gcube.social_networking.socialnetworking.model.beans.UserProfile;
import org.gcube.social_networking.socialnetworking.model.beans.UserProfileExtended;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEvent;
import org.gcube.social_networking.socialnetworking.model.beans.catalogue.CatalogueEventType;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.AddedItemEvent;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.DeletedItemEvent;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.FileItemBean;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.FolderBean;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.UpdatedItemEvent;
import org.gcube.social_networking.socialnetworking.model.beans.workspace.WorkspaceEvent;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestClientServices {

	private static Logger logger = LoggerFactory.getLogger(TestClientServices.class);

	@SuppressWarnings("deprecation")
	@Before
	public void setContextAndToken(){

		//ScopeProvider.instance.set("/d4science.research-infrastructures.eu/gCubeApps/gCube");
		//String umaToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJySUJPYjZZY3p2ZE4xNVpuNHFkUTRLdEQ5VUhyY1dwNWJCT3NaLXpYbXM0In0.eyJleHAiOjE2NjM2NjU2NTgsImlhdCI6MTY2MzY2NTM1OCwiYXV0aF90aW1lIjoxNjYzNjY0MTg1LCJqdGkiOiIxMjNhYmY0MS1lZmE1LTQ4ZjEtYmE2Zi03MjNkOWZjNDAwMWQiLCJpc3MiOiJodHRwczovL2FjY291bnRzLmQ0c2NpZW5jZS5vcmcvYXV0aC9yZWFsbXMvZDRzY2llbmNlIiwiYXVkIjoiJTJGZDRzY2llbmNlLnJlc2VhcmNoLWluZnJhc3RydWN0dXJlcy5ldSUyRmdDdWJlQXBwcyUyRmdDdWJlIiwic3ViIjoiMzM4OGQwZjgtM2E0OS00ZGEwLWE3OGUtN2I2MjI1OTI2M2U2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidmxhYi5pc3RpLmNuci5pdCIsInNlc3Npb25fc3RhdGUiOiI3MjRkMmJmOS00NjQ2LTQwZjAtOTY1ZC02MzFlODJiZWZkMmEiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIi8qIl0sInJlc291cmNlX2FjY2VzcyI6eyIlMkZkNHNjaWVuY2UucmVzZWFyY2gtaW5mcmFzdHJ1Y3R1cmVzLmV1JTJGZ0N1YmVBcHBzJTJGZ0N1YmUiOnsicm9sZXMiOlsiVlJFLU1hbmFnZXIiLCJNZW1iZXIiXX19LCJhdXRob3JpemF0aW9uIjp7InBlcm1pc3Npb25zIjpbeyJyc2lkIjoiYWNlM2ZmNWQtYjU2ZS00MDgzLTljMzAtMjY0NTJiODc3YWIzIiwicnNuYW1lIjoiRGVmYXVsdCBSZXNvdXJjZSJ9XX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiTWFzc2ltaWxpYW5vIEFzc2FudGUiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJtYXNzaW1pbGlhbm8uYXNzYW50ZSIsImdpdmVuX25hbWUiOiJNYXNzaW1pbGlhbm8iLCJmYW1pbHlfbmFtZSI6IkFzc2FudGUiLCJlbWFpbCI6Im1hc3NpbWlsaWFuby5hc3NhbnRlQGlzdGkuY25yLml0In0.KIxC9QYGZCp6jAdye_82q648JjZli9KMxe-lqyFWkuA-HaZ-Ig2lWyn747iKp3UmstQgCTTonmOsVANHp1Feu_U1CuiWqRZ8OhmrTj8Q5v-FKwVtN2GfbjOF9b4aMXySFPd1HtCGHJ4o57uUrIQvvOV_SJOK5SOjG0YzOmsrOcXzSPl97ZZLKwio-Py0rxN6fdK8Obx7TL1eGgllhAI7ZDFRfoZrbz-F1YL1IPlQ6RI76rb7sbt6oL-T6LirP92AmUaW_nTLBBqrFR7uCaZdZKIDd4zxBmyzXjkNHncMKg8yFl-i1SRe58EcucMwEN0O-kUkhIb2CzaClsiWcLjP5g";
		ScopeProvider.instance.set("/gcube/devsec/devVRE");
		String umaToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NjYyODUxMDYsImlhdCI6MTY2NjI4NDgwNiwiYXV0aF90aW1lIjoxNjY2Mjc5NDM2LCJqdGkiOiJjYjlmZDgxNi1kNGU5LTQ4NTYtOTQ5My1iYzk4ZDFhYTdiZTMiLCJpc3MiOiJodHRwczovL2FjY291bnRzLmRldi5kNHNjaWVuY2Uub3JnL2F1dGgvcmVhbG1zL2Q0c2NpZW5jZSIsImF1ZCI6IiUyRmdjdWJlJTJGZGV2c2VjJTJGZGV2VlJFIiwic3ViIjoiNzcxZjYxNTEtMDBhZS00NWMyLWE3NTQtZjA1NDZkOThmNDgyIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibmV4dC5kNHNjaWVuY2Uub3JnIiwic2Vzc2lvbl9zdGF0ZSI6ImNlZDM0NmE1LWExNzctNGNmYy1iYjU5LTBlMjk5OWJhZDU4ZCIsImFsbG93ZWQtb3JpZ2lucyI6WyIvKiJdLCJyZXNvdXJjZV9hY2Nlc3MiOnsiJTJGZ2N1YmUlMkZkZXZzZWMlMkZkZXZWUkUiOnsicm9sZXMiOlsiQ2F0YWxvZ3VlLU1hbmFnZXIiLCJNZW1iZXIiXX19LCJhdXRob3JpemF0aW9uIjp7InBlcm1pc3Npb25zIjpbeyJyc2lkIjoiNTcyODU1MTAtMzkzOS00ZGU3LThmYzEtZTNhOWQzY2NlMjgxIiwicnNuYW1lIjoiRGVmYXVsdCBSZXNvdXJjZSJ9XX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsInNpZCI6ImNlZDM0NmE1LWExNzctNGNmYy1iYjU5LTBlMjk5OWJhZDU4ZCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiQW5kcmVhIFJvc3NpIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYW5kcmVhLnJvc3NpIiwiZ2l2ZW5fbmFtZSI6IkFuZHJlYSIsImZhbWlseV9uYW1lIjoiUm9zc2kiLCJlbWFpbCI6Im0uYXNzYW50ZUBnbWFpbC5jb20ifQ.HHHqPWPkq8wVV2dT8U4iraGy0VpdFb2MJKTGZLz3woqhGOo7cARKTF9S_Cv9RigkL7vg_vriScDZzwCH-jVopK8EDRZTthiQIITzYG7EPg-B0zuOkUeC1GE0LFU3kqCTpwbLElOmQJfn6FP95lzMQWIhJrMSwVwjsXDf12bcYyr0QZDa8A9lxA4RkQezkpXhJ72s841oOp1vul5wyoSFYCdr7cZnMLuY5pdasheqGfQdkGi39SZ_dlnGgAkEArVqUCIlCxvdT2EUpiPG9854e2fBFdOss6NiSbZHF5n5nLvR8kDCaN12YOCw54fxwAByhvI0pBKEa_uM029eip43-A";
		umaToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NjYzNDYzNDYsImlhdCI6MTY2NjM0NjA0NiwianRpIjoiNDBjNjk2YmQtNjdiMi00MGQ4LTk4OGEtZjk2OTMzMDdlMzJkIiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5kZXYuZDRzY2llbmNlLm9yZy9hdXRoL3JlYWxtcy9kNHNjaWVuY2UiLCJhdWQiOiIlMkZnY3ViZSUyRmRldnNlYyUyRmRldlZSRSIsInN1YiI6IjUyOGI2M2I1LTAwYTgtNGQ0My1hMWRlLTE1NDMyYTAxZjNiMSIsInR5cCI6IkJlYXJlciIsImF6cCI6Im5leHQuZDRzY2llbmNlLm9yZyIsInNlc3Npb25fc3RhdGUiOiJmMjYyNTQyMC03NGM1LTQ1NDQtYjY2YS1kZmEwOGQ2N2Q0NTQiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVzb3VyY2VfYWNjZXNzIjp7ImNvbmR1Y3Rvci1zZXJ2ZXIiOnsicm9sZXMiOlsiY29uZHVjdG9yLW1hbmFnZXIiXX0sIiUyRmdjdWJlJTJGZGV2c2VjJTJGZGV2VlJFIjp7InJvbGVzIjpbIkNhdGFsb2d1ZS1NYW5hZ2VyIiwiQ2F0YWxvZ3VlLU1vZGVyYXRvciIsIk1lbWJlciJdfX0sImF1dGhvcml6YXRpb24iOnsicGVybWlzc2lvbnMiOlt7InJzaWQiOiI1NzI4NTUxMC0zOTM5LTRkZTctOGZjMS1lM2E5ZDNjY2UyODEiLCJyc25hbWUiOiJEZWZhdWx0IFJlc291cmNlIn1dfSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiZjI2MjU0MjAtNzRjNS00NTQ0LWI2NmEtZGZhMDhkNjdkNDU0IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJMdWNhIEZyb3NpbmkiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJsdWNhLmZyb3NpbmkiLCJnaXZlbl9uYW1lIjoiTHVjYSIsImZhbWlseV9uYW1lIjoiRnJvc2luaSIsImVtYWlsIjoibHVjYS5mcm9zaW5pQGlzdGkuY25yLml0In0.jt43mGeSm_4hVNFThTBmmOxzotsTHPp0DKDzBNAgESFkzNRb7ZE1j7Ab_1M6fqjpciLsk093XTEpws0OksUEAWUgxj5iAOXopJKRr6cCnmQXitWTa8WpwWZyj6AiI8PSgHCQhY3-CU8mptmd4acDtagHSbhZ320aNx7rLTaZ_7KEdRmErmgzWhnPwD3O2ONcnxIuAL1rqu4o8SJl-cj5ppFc2V54VSY56AT2IK2GmP_he9R32_0K_F0SAwsuDmv7eF1MwQM7j27H9ewfEKP8w9RooZtZYj3Pm7-Ac4XSir9w5eG1xKOyiGbIlvJZU4FQwXINntUVTVPVfPaXZ_ZbOw";
		AccessTokenProvider.instance.set(umaToken);
		SecurityTokenProvider.instance.set("");
	}

	
	@Test
	public void testCatalogueEvents() throws Exception {
		NotificationClient nc = new NotificationClient();
		String[] idstoNotify = {"andrea.rossi"};
		CatalogueEvent event = 
				new CatalogueEvent(
						CatalogueEventType.ITEM_SUBMITTED, 
						idstoNotify, 
						"Submitted item RESTful Transaction Model", 
						"Giorgino Giorgetti created the item RESTful Transaction Model. You are kindly requested to review it and decide either to APPROVE or REJECT it. ", 
						new URL("https://data-dev.d4science.net/rU7T"));
		nc.sendCatalogueEvent(event);
		
		
	}
	
	@Test
	public void testWorkspaceItemEvents() throws Exception {
		NotificationClient nc = new NotificationClient();
		boolean vreFolder = false;
		FolderBean folder = new FolderBean("217cbe15-6408-41e5-9fda-628f56b8e803", "folderName", "folderTitle","displayName",
				"/Workspace/_shared attachments", "90b8da48-d363-441a-a0a2-6f614ae747b9", vreFolder);
		FileItemBean fileItem =	new FileItemBean("90b8da48-d363-441a-f0a2-4f413ae747d7", "updated item.pdf", "updated item.pdf", 
				"/Workspace/_shared attachments/test updated item from client", folder);
		
		String[] idstoNotify = {"andrea.rossi"};
		WorkspaceEvent event = new UpdatedItemEvent(idstoNotify, fileItem);				
		nc.sendWorkspaceEvent(event);
		event = new AddedItemEvent(idstoNotify, fileItem);		
		nc.sendWorkspaceEvent(event);
		event = new DeletedItemEvent(idstoNotify, "theDeletedFilenName.pdf", folder);			
		nc.sendWorkspaceEvent(event);
		
		String[] contextIdstoNotify = {"/pred4s/preprod/preVRE"};
		event = new UpdatedItemEvent(contextIdstoNotify, true, fileItem);				
		nc.sendWorkspaceEvent(event);
	}

	
	@Test
	public void testSearch() throws Exception {
		FullTextSearchClient search = new FullTextSearchClient();
		ArrayList<EnhancedFeed> result = search.search("looking for test", 0, 10);

		for (EnhancedFeed enhancedPost : result) {
			logger.debug("Returned post " + enhancedPost);
		}
	}

	//@Test
	public void testSearchEmptyList() throws Exception {
		FullTextSearchClient search = new FullTextSearchClient();
		ArrayList<EnhancedFeed> result = search.search("tipiterotipirè", 0, 10);

		for (EnhancedFeed enhancedFeed : result) {
			logger.debug("Returned feed " + enhancedFeed);
		}
	}

	@Test
	public void testHashTags() throws Exception {
		HashTagClient hashtags = new HashTagClient();
		Map<String, Integer> hashtagsCount = hashtags.getHashtagsCount();
		logger.debug("Returned tags " + hashtagsCount);
	}

	@Test
	public void testMessagesReceived() throws Exception {
		MessageClient messagesClient = new MessageClient();
		List<MessageInbox> receivedMessages = messagesClient.getReceivedMessages();
		System.out.println("Messages found # " + receivedMessages.size());
		for (MessageInbox workspaceMessage : receivedMessages) {
			logger.debug("Received message  " + workspaceMessage.toString());
		}
	}

	@Test
	public void testMessagesSent() throws Exception {
		MessageClient messagesClient = new MessageClient();
		List<MessageInbox> sentMessages = messagesClient.getSentMessages();
		for (MessageInbox workspaceMessage : sentMessages) {
			logger.debug("Sent message was " + workspaceMessage);
		}
	}

	@Test
	public void sendMessage() throws Exception{
		logger.debug("Sending message ");
		MessageClient messagesClient = new MessageClient();
		List<Recipient> rec = Arrays.asList(new Recipient("andrea.rossi"), new Recipient("giancarlo.panichi"));
		MessageInputBean message = new MessageInputBean(
				"Test message", 
				"Sending message via client " + System.currentTimeMillis(), 
				new ArrayList<Recipient>(rec));
		String idMessage = messagesClient.writeMessage(message);
		assert(idMessage != null);
	}
	
	@Test
	public void markMessageRead() throws Exception{
		logger.debug("markMessageRead message ");
		MessageClient messagesClient = new MessageClient();
		String idMessage = messagesClient.markMessageRead("a7b825c3-a0ca-42d7-b228-00b9e8f015e4", false);
		assert(idMessage != null);
	}
	
	

	//@Test
	public void testNotifications() throws Exception {
		NotificationClient notificationsClient = new NotificationClient();
		List<Notification> latestNotifications = notificationsClient.getNotifications(1, 2);
		for (Notification notification : latestNotifications) {
			logger.debug("Notification is " + notification);
		}
	}

	@Test
	public void sendJobNotification() throws Exception {
		NotificationClient notificationsClient = new NotificationClient();
		JobNotificationBean notification = new JobNotificationBean(
				"luca.frosini", 
				UUID.randomUUID().toString(), 
				"SmartExecutor Social Indexer", 
				"SmartExecutor", 
				JobStatusModelType.SUCCEEDED, 
				"all ok");
		notificationsClient.sendJobNotification(notification);
		logger.debug("Sent job notification ");
	}

	@Test
	public void getProfile() throws Exception {
		PeopleClient getProfile = new PeopleClient();
		UserProfile profile = getProfile.getProfile();
		logger.debug("Profile retrieved is " + profile);
	}

	//@Test
	public void generateAppToken() throws Exception {
		TokenClient tokenClient = new TokenClient();
		String token = tokenClient.generateApplicationToken(new ApplicationId("org.gcube.datacatalogue.GRSFNotifier"));
		logger.debug("Generated token is  " + token);
	}

	//@Test
	public void getMyVres() throws Exception{
		VREClient myVresClient = new VREClient();
		List<MyVRE> myVres = myVresClient.getMyVRES(false);
		logger.debug("My Vres  " + myVres);
	}

	@Test
	public void TestUsersClient() throws Exception{

		UserClient userClient = new UserClient();
		Set<String> vreManagers = userClient.getAllUsernamesByRole("Data-Manager");
		assert(vreManagers != null && !vreManagers.isEmpty());
		System.out.println("Data Managers are " + vreManagers);
		
		List<String> moderators = userClient.getAllUsernamesByLocalRole("VRE-Managers");
		assert(moderators != null && !moderators.isEmpty());
		System.out.println("Catalogue-Moderators are " + moderators);
		
		

		String myEmail = userClient.getEmail();

		assert(myEmail != null && !myEmail.isEmpty());

		logger.debug("My email is " + myEmail);

		String fullname = userClient.getFullName();

		assert(fullname != null && !fullname.isEmpty());

		logger.debug("My fullname is " + fullname);

		String industry = userClient.getCustomAttribute("Industry");

		//assert(industry != null && !industry.isEmpty());

		logger.debug("My Industry is " + industry);

		List<String> usernamesInThisContext = userClient.getAllUsernamesContext();

		assert(usernamesInThisContext != null && !usernamesInThisContext.isEmpty());

		logger.debug("List of usernames in this context is  " + usernamesInThisContext);

		Map<String, String> usernamesFullnameInThisContext = userClient.getAllUsernamesFullnamesContext();

		assert(usernamesFullnameInThisContext != null && !usernamesFullnameInThisContext.isEmpty());

		logger.debug("List of tuples usernames/fullnames in this context is  " + usernamesFullnameInThisContext);

		UserProfileExtended profile = userClient.getProfile();

		assert(profile != null);

		logger.debug("My wonderful profile is " + profile);

		//		Requires application token bound to the root context
		//				Boolean userExists = userClient.userExists("andrea.rossi");
		//				
		//				assert(userExists != null && userExists == true);
		//				
		//				logger.debug("does Andrea Rossi exist? " + userExists);

	}

	@Test
	public void getUserPostsSinceDate() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> sinceLastYearPost = postClient.getUserPostsSinceDate(System.currentTimeMillis() - 1000 * 60 * 60 * 24 * 12);
		logger.debug("Posts are " + sinceLastYearPost);

	}

	@Test
	public void getAllUserPosts() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> allposts = postClient.getAllUserPosts();
		logger.debug("All posts found:");
		for (Post post : allposts) {
			logger.debug(post.toString());
		}
		

	}


	@Test
	public void getUserPostsQuantity() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> quantityPosts = postClient.getUserPostsQuantity(3);
		logger.debug("Some posts are ");
		logger.debug(quantityPosts + " posts found:");
		for (Post post : quantityPosts) {
			logger.debug(post.toString());
		}
	}

	@Test
	public void writeUserPost() throws Exception{

		PostClient postClient = new PostClient();
		PostInputBean postInputBean = new PostInputBean();
		postInputBean.setText("Testing social networking rest client at " + new Date());
		//Post post = postClient.writeApplicationPost(postInputBean);
		PostInputBean post = new PostInputBean("Testing social networking rest client", null, null, null, null, null, false, null);
		Post written = postClient.writeUserPost(postInputBean);
		logger.debug("Written post is " + post);

	}

	//@Test
	public void getAllApplicationPosts() throws Exception{

		TokenClient tokenClient = new TokenClient();
		String token = tokenClient.generateApplicationToken(new ApplicationId("org.gcube.datacatalogue.GRSFNotifier"));
		logger.debug("Generated token is  " + token);

		String currentToken = SecurityTokenProvider.instance.get();
		SecurityTokenProvider.instance.set(token);

		PostClient postClient = new PostClient();
		List<Post> applicationPosts = postClient.getAllApplicationPosts();
		logger.debug("Application posts are " + applicationPosts);

		SecurityTokenProvider.instance.set(currentToken);

	}

	//@Test
	public void writeApplicationPost() throws Exception{

		TokenClient tokenClient = new TokenClient();
		String token = tokenClient.generateApplicationToken(new ApplicationId("org.gcube.datacatalogue.GRSFNotifier"));
		logger.debug("Generated token is  " + token);

		String currentToken = SecurityTokenProvider.instance.get();
		SecurityTokenProvider.instance.set(token);

		PostClient postClient = new PostClient();
		PostInputBean toWrite = new PostInputBean("Testing social networking rest client [via application token] #apptoken #rest #client #java", null, null, null, null, null, false, null);
		Post written = postClient.writeApplicationPost(toWrite);
		logger.debug("Written post is " + written);

		SecurityTokenProvider.instance.set(currentToken);

	}

	@Test
	public void getPostsVRE() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> vrePosts = postClient.getVREPosts();
		logger.debug("VRE posts : ");
		for (Post post : vrePosts) {
			logger.debug(post.toString());
		}

	}

	//@Test
	public void getHashtagPosts() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> postsWithHashtag = postClient.getHashtagPosts("#connect");
		logger.debug("Posts with hashtag #connect are " + postsWithHashtag);

	}

	@Test
	public void getUserLikedPost() throws Exception{

		PostClient postClient = new PostClient();
		List<Post> postsLiked = postClient.getUserLikedPost();
		logger.debug("Posts liked are " + postsLiked);

	}

	@Test
	public void getUserLikedPostIds() throws Exception{

		PostClient postClient = new PostClient();
		List<String> postsLikedIds = postClient.getUserLikedPostIds();
		logger.debug("Posts liked's ids are " + postsLikedIds);

	}


	//@After
	public void reset(){
		ScopeProvider.instance.reset();
		SecurityTokenProvider.instance.reset();
	}
}
